all: prog


test: test.o header.o
	g++ test.o header.o -g -o test
test.o: test.cpp
	g++ test.cpp -c -g -o test.o
prog: header.o main.o
	g++ header.o main.o -g -o prog
header.o: header.cpp header.hpp
	g++ header.cpp -c -g -o header.o
main.o: main.cpp
	g++ main.cpp -c -g -o main.o
clean:
	rm *.o
	rm prog
