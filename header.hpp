#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

class CVector
{
protected:
	double *array;
	int n;
public:
	CVector(): array(NULL), n(0){}
	CVector(int _n): array(new double[_n]), n(_n) {}
	CVector(const CVector& Q);
	virtual ~CVector(void) { if(array != NULL) delete[] array; array = NULL;}
	CVector& operator=(const CVector& Q);
	CVector operator+(const CVector& Q);
	CVector operator-(const CVector& Q);
	double operator*(const CVector& Q);
	double& operator[](int i);
	virtual void Save(const string& filename)=0;//{cout << filename << endl;}
};

class CVector_1 : public CVector
{
public:
	CVector_1(int _n) : CVector(_n){};
	void Save(const string& filename);
};

class CVector_2 : public CVector
{
public:
	CVector_2(int _n) : CVector(_n){}
	void Save(const string& filename);
};

void LoadFromFile(const string& filename, CVector*** arr, int &N);
