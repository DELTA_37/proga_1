#include "header.hpp"

CVector::CVector(const CVector& Q)
{
	this->n = Q.n;
	if (Q.array == NULL)
	{
		this->array = NULL;
	}
	else
	{
		int i;
		this->array = new double[Q.n];
		for (i = 0; i <= Q.n - 1; i++)
		{
			this->array[i] = Q.array[i];
		}
	}
}

CVector& CVector::operator=(const CVector& Q)
{
	int i;
	this->n = Q.n;
	if (this->array != NULL)
	{
		delete[] this->array;
	}
	if (Q.array == NULL)
	{
		this->array = NULL;
		this->n = 0;
		return *this;
	}
	this->array = new double[Q.n];
	for (i = 0; i < Q.n; i++)
	{
		this->array[i] = Q.array[i];
	}
	return *this;
}

CVector CVector::operator+(const CVector& Q)
{
	int i;
	assert(this->n == Q.n);
	CVector res(Q.n);
	for (i = 0; i <= Q.n - 1; i++)
	{
		res.array[i] = this->array[i] + Q.array[i];
	}
	return res;
}

CVector CVector::operator-(const CVector& Q)
{
	int i;
	assert(this->n == Q.n);
	CVector res(Q.n);
	for (i = 0; i <= Q.n - 1; i++)
	{
		res.array[i] = this->array[i] - Q.array[i];
	}
	return res;
}

double CVector::operator*(const CVector& Q)
{
	int i;
	assert(this->n == Q.n);
	double s = 0;
	for (i = 0; i <= Q.n - 1; i++)
	{
		s += this->array[i]*Q.array[i];
	}
	return s;
}

void CVector_1::Save(const string& filename)
{
	int i;
	ofstream out;
	out.open(filename.c_str(), fstream::app | fstream::out);
	out << "1 " << this->n << endl;
	if (this->n != 0)
	{
		for (i = 0; i <= this->n - 1; i++)
		{
			out << this->array[i] << " ";
		}
	}
	out << endl;
	out.close();
}

void CVector_2::Save(const string& filename)
{
	int i;
	ofstream out;
	out.open(filename.c_str(), fstream::app | fstream::out);
	out << "2 " << this->n << std::endl;
	if (this->n != 0)
	{
		for (i = 0; i <= n - 1; i++)
		{
			out << this->array[i] << std::endl;
		}
	}
	out << endl;
	out.close();
}

double& CVector::operator[](int i)
{
	assert(i <= this->n - 1);
	assert(i >= 0);
	return this->array[i];
}

void LoadFromFile(const string& filename, CVector*** arr, int &N)
{
	int i, j;
	std::ifstream in;
	in.open(filename.c_str());
	in >> N;
	(*arr) = new CVector*[N];
	for (i = 0; i < N; i++)
	{
		int type;
		int n;
		in >> type >> n;
		if (type == 1)
		{
			(*arr)[i] = new CVector_1(n);
		}
		else
		{
			(*arr)[i] = new CVector_2(n);
		}
		for (j = 0; j < n; j++)
		{
			in >> (*(*arr)[i])[j];
		}
	}
	in.close();
}
