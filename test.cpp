#include "header.hpp"
#include <cstdlib>

using namespace std;

void generateData(int n, const string& filename)
{
	ofstream out;
	out.open(filename.c_str());
	for (int i = 0; i < n; i++)
	{
		int type = random()%2 + 1;
		int N = random()%100 + 1;
		out << type << " " << N << endl;
		for (int j = 0; j < N; j++)
		{
			out << random()%100 << endl;
		}
		out << endl;
	}
	out.close();
}

int main(void)
{
	int n;
	cin >> n;
	generateData(n, "in.txt");
	return 0;
}
